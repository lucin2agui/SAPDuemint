﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due
{
    class CDocuments
    {
        public void GetDocuments()
        {
            //string mensaje = null;
            try
            {
                var clientDowload = new RestClient();
                string URLDowload = Properties.Settings.Default.URLDoc;

                var request = new RestRequest(URLDowload, Method.GET);

                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Cookie", "device_view=full");
                Parameter Param = new Parameter();
                Param.Name = "since";
                Param.Value = DateTime.Now.ToString("yyyy-MM-dd");
                request.Parameters.Add(Param);

                request.RequestFormat = DataFormat.Json;
                IRestResponse responseDowload = clientDowload.Execute(request);

                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    var Doc = responseDowload.Content;

                    IntSAP_Due.Common2.lDoc lDoc = JsonConvert.DeserializeObject<IntSAP_Due.Common2.lDoc>(responseDowload.Content);
                    if (lDoc.Records.items > 0)
                    {      
                        foreach (IntSAP_Due.Common2.items doc in lDoc.items)
                        {
                            IntSAP_Due.Common.LOGSAP log = new IntSAP_Due.Common.LOGSAP();
                            IntSAP_Due.Common.INTSAPDUE oTableLog = new IntSAP_Due.Common.INTSAPDUE();
                            oTableLog.Code = doc.id.ToString() + DateTime.Now.ToString("yyyyMMddmm");
                            oTableLog.Name = doc.id.ToString();
                            oTableLog.U_IdDueMint = int.Parse(doc.id);
                            oTableLog.U_ObjType = 13;
                            oTableLog.U_Json = doc.ToString();
                            log.AddLogSap(oTableLog);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public IntSAP_Due.Common2.items GetDocument(string Folio)
        {
            IntSAP_Due.Common2.items document = new Common2.items();
            try
            {
                var clientDowload = new RestClient();
                string URLDowload = Properties.Settings.Default.URLDoc;

                var request = new RestRequest(URLDowload, Method.GET);

                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Cookie", "device_view=full");
                Parameter Param = new Parameter();
                Param.Name = "number";
                Param.Value = Folio;
                request.Parameters.Add(Param);

                request.RequestFormat = DataFormat.Json;
                IRestResponse responseDowload = clientDowload.Execute(request);

                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    var Doc = responseDowload.Content;

                  var lDoc = JsonConvert.DeserializeObject<IntSAP_Due.Common2.lDoc>(responseDowload.Content);
                    if (lDoc.Records.items > 0)
                    {
                        foreach (IntSAP_Due.Common2.items doc in lDoc.items)
                        {
                            document = doc;      
                            var id = doc.id.ToString();
                            var ClientId = doc.Client.taxId;
                            var Json = doc.ToString();
                        }

                    }
                }
                return document;
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
                return document;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
