﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common.Due
{
    public class PayDuemint
    {
        public string taxId { get; set; }
        public string date { get; set; }
        public string total { get; set; }
        public string currency { get; set; }  
        public string paymentMethod { get; set; }
        public PayDuemint()
        {
            this.documents = new List<IntSAP_Due.Common.Due.documents>();
        }

        public List<IntSAP_Due.Common.Due.documents> documents { get; set; }
    }

    public class documents
    {
        public string number { get; set; }
        public string code { get; set; }
        public string amount { get; set; }
    }
}
