﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common2
{
    public class items
    {
        public string id { get; set; }
        public string clientTaxId { get; set; }
        public string number { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime issueDate { get; set; }
        public DateTime dueDate { get; set; }
        public int status { get; set; }
        public string statusName { get; set; }
        public string currency { get; set; }
        public double net { get; set; }
        public double taxes { get; set; }
        public double total { get; set; }
        public double paidAmount { get; set; }
        public DateTime? paidDate { get; set; }
        public double amountDue { get; set; }
        public double amountCredit { get; set; }
        public double amountDebit { get; set; }
        public string purchaseOrder { get; set; }
        public string gloss { get; set; }
        public string notes { get; set; }
        public int code { get; set; }
        public string url { get; set; }
        public string xml { get; set; }
        public string pdf { get; set; }
        public client Client { get; set; }
        public string[] tags { get; set; }
        public string[] files { get; set; }
    }
    public class records
    {
        public int totalRecords { get; set; }
        public int items { get; set; }
        public int page { get; set; }
        public int pages { get; set; }
    }
    public class client
    {
        public string id { get; set; }
        public string name { get; set; }
        public string taxId { get; set; }

    }
    public class lDoc
    {
        public records Records { get; set; }
        public IList<items> items { get; set; }
    }
}
