﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common
{
    public class Error
    {
        public int error { get; set; }
        public string message { get; set; }
    }
}
