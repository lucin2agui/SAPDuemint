﻿using IntSAP_Due.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due
{
    public class CPayments
    {
        public void GetPayments()
        {
            var clientDowload = new RestClient();
            string URLDowload;
            string param;
            string param2;
            LOGSAP log = new LOGSAP();
            INTSAPDUE oTableLog = new INTSAPDUE();
            try
            {
                URLDowload = Properties.Settings.Default.URLPay;
                param = "?since=" + (string.IsNullOrEmpty(Properties.Settings.Default.FechaPagos) == true ? DateTime.Now.ToString("yyyy-MM-dd") : Properties.Settings.Default.FechaPagos);
                param2 = "&dateBy=3&order=ASC&resultsPerPage=100&status=1";
                var request = new RestRequest(URLDowload + param + param2, Method.GET);
                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Cookie", "device_view=full");
                request.RequestFormat = DataFormat.Json;
                IRestResponse responseDowload = clientDowload.Execute(request);
                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    Common.lPay lPay = JsonConvert.DeserializeObject<Common.lPay>(responseDowload.Content);
                    if (lPay.Records.items > 0)
                    {
                        foreach (items pay in lPay.items)
                        {
                            var josn = JsonUtility.JsonParser.Serialize(pay, true);
                            oTableLog.Code = pay.id.ToString();
                            oTableLog.Name = "Pago desde Duemint " + pay.id.ToString();
                            oTableLog.U_IdDueMint = pay.id;
                            oTableLog.U_ObjType = 24;
                            oTableLog.U_Json = josn.ToString();
                            oTableLog.U_Status = 1;
                            log.AddLogPay(oTableLog);
                            //log.AddLogSap(oTableLog);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                clientDowload = null;
                URLDowload = null;
                param = null;
                param2 = null;
                log = null;
                clientDowload = null;
                oTableLog = null;
                GC.Collect();
            }
        }

        public void SentPaymentsDue()
        {
            Conex con = new Conex();
            {
                string Query = null;
                string Query2 = null;
                string Code = null;
                string DocEntry = null;
                double DocTotal;
                double SumApplied;
                IntSAP_Due.Common.Due.PayDuemint cCPay = new IntSAP_Due.Common.Due.PayDuemint();
                try
                {
                    Query = @"SELECT TOP 20 T0.[Code]
	                              ,T0.[U_DocEntry]
	                              ,T3.DocTotal
	                              ,T3.TaxDate
	                              ,T4.LicTradNum
                            FROM [@INTSAPDUE] T0 WITH (NOLOCK)
                            INNER JOIN ORCT T3 WITH (NOLOCK) ON CONVERT(VARCHAR(30),T3.[DocEntry]) = CONVERT(VARCHAR(30),T0.[U_DocEntry]) 
                            INNER JOIN OCRD T4 WITH (NOLOCK) ON T4.CardCode = T3.CardCode
                            WHERE T0.[U_Status] = 1
                            AND T0.[Name] LIKE '%Pago desde SAP%'
                          
                        ";
                    var PayDue = con.exeReader(Query);
                    if (PayDue.FieldCount > 0)
                    {
                        // IntSAP_Due.Common.Due.PayDuemint cCPay = new IntSAP_Due.Common.Due.PayDuemint();
                        while (PayDue.Read())
                        {
                            Code = PayDue["Code"].ToString();
                            DocEntry = Convert.ToString(PayDue["U_DocEntry"]);
                            DocTotal = Math.Round(Convert.ToDouble(PayDue["DocTotal"]), 0);
                            cCPay = new Common.Due.PayDuemint();
                            cCPay.currency = "CLP";
                            cCPay.date = Convert.ToDateTime(PayDue["TaxDate"]).ToString("yyyy-MM-dd");
                            cCPay.total = DocTotal.ToString();
                            cCPay.taxId = Convert.ToString(PayDue["LicTradNum"]);
                            //detalle
                            Query2 = @" SELECT 
	                                 T1.[DocNum]
	                                ,T1.[InvoiceId]
	                                ,T1.[DocEntry]
	                                ,T1.[SumApplied]
	                                ,T1.[AppliedSys]
	                                ,T2.[FolioNum]
                                    ,T3.[CheckSum] 
                                    ,T3.[TrsfrSum]
                                FROM RCT2 T1 WITH (NOLOCK) 
                                INNER JOIN ORCT T3 WITH (NOLOCK) ON T3.[DocEntry] = T1.[DocNum]
                                INNER JOIN OINV T2 WITH (NOLOCK) ON T2.[DocEntry] = T1.[DocEntry]
                                WHERE
                                CONVERT(VARCHAR(30),T1.[DocNum]) = CONVERT(VARCHAR(30), '" + DocEntry + @"')
                                    ";
                            var DocDue = con.exeReader(Query2);
                            while (DocDue.Read())
                            {
                                if (Convert.ToDouble(DocDue["CheckSum"]) > 0)
                                {
                                    cCPay.paymentMethod = "Cheque";
                                }
                                else
                                {
                                    cCPay.paymentMethod = "Transferencia";
                                }
                                SumApplied = Math.Round(Convert.ToDouble(DocDue["SumApplied"].ToString()), 0);
                                cCPay.documents.Add(new IntSAP_Due.Common.Due.documents() { number = Convert.ToString(DocDue["FolioNum"]), code = "33", amount = SumApplied.ToString() });
                            }
                            CreatePaymentByTaxId(cCPay, Code);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Common.LogArchivo.EscribeLog(ex);
                }
                finally
                {
                    con = null;
                    Query = null;
                    Query2 = null;
                    Code = null;
                    DocEntry = null;
                    DocTotal = 0;
                    SumApplied = 0;
                    cCPay = null;
                    GC.Collect();
                }
            }
        }

        public void CreatePaymentByTaxId(IntSAP_Due.Common.Due.PayDuemint cCPay, string code)
        {           
            try
            {
                string body = JsonConvert.SerializeObject(cCPay);

                IntSAP_Due.Common.Paydownload.Paydownload paydownload = new IntSAP_Due.Common.Paydownload.Paydownload();
                string URLCP = Properties.Settings.Default.URLCPBT;
                var clientRes = new RestClient(URLCP);                
                var request = new RestRequest( Method.POST);
                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Cookie", "device_view=full");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //request.RequestFormat = DataFormat.Json;
                //request.AddHeader("Accept", "application/json");
                //request.AddParameter("taxId", cCPay.taxId.ToString());
                //request.AddParameter("date", cCPay.date);
                //request.AddParameter("total", cCPay.total);
                //request.AddParameter("currency", cCPay.currency);
                //string doc = JsonConvert.SerializeObject(cCPay.documents).ToString();
                //request.AddParameter("documents", doc.Replace(@"\", ""));

                //request.AddBody(JsonPay);

                IRestResponse responseDowload = clientRes.Execute(request);
                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    paydownload = JsonConvert.DeserializeObject<IntSAP_Due.Common.Paydownload.Paydownload>(responseDowload.Content);
                    if (paydownload.id > 0)
                    {
                        var josn = JsonUtility.JsonParser.Serialize(paydownload, true);
                        LOGSAP log = new LOGSAP();
                        INTSAPDUE oTableLog = new INTSAPDUE();
                        oTableLog.Code = code;
                        oTableLog.U_IdDueMint = paydownload.id;
                        oTableLog.U_Mensaje = "Creado";
                        oTableLog.U_Json = josn.ToString();
                        oTableLog.U_JsonSent = body;
                        oTableLog.U_Status = 2;
                        log.UpdateLogSap(oTableLog);
                    }
                    else
                    {
                        var mens = JsonConvert.DeserializeObject<IntSAP_Due.Common.Error>(responseDowload.Content);

                        LOGSAP log = new LOGSAP();
                        INTSAPDUE oTableLog = new INTSAPDUE();
                        oTableLog.Code = code;
                        oTableLog.U_IdDueMint = 0;
                        oTableLog.U_Mensaje = "Enviado sin respuesta";
                        oTableLog.U_Json = "";
                        oTableLog.U_JsonSent = body;
                        oTableLog.U_Status = 2;
                        log.UpdateLogSap(oTableLog);
                    }
                }
                else
                {
                    var mens = JsonConvert.DeserializeObject<IntSAP_Due.Common.Error>(responseDowload.Content);

                    LOGSAP log = new LOGSAP();
                    INTSAPDUE oTableLog = new INTSAPDUE();
                    oTableLog.Code = code;
                    oTableLog.U_IdDueMint = 0;
                    oTableLog.U_Mensaje = "Error:" + mens.error + " mensaje :" + mens.message;
                    oTableLog.U_Json = "";
                    oTableLog.U_JsonSent = body;
                    oTableLog.U_Status = 3;
                    log.UpdateLogSap(oTableLog);
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
