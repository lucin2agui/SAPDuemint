﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace IntSAP_Due
{
    public partial class ServInt : ServiceBase
    {
        private DateTime vig = new DateTime(2022, 03, 04);
        private Timer t = null;
        public ServInt()
        {
            InitializeComponent();
            //t = new Timer(60000);  //60 SEGUNDOS
            t = new Timer(1000 * 30 * 10);  //30=5 minutos
            t.Elapsed += new ElapsedEventHandler(t_Elapsed);
        }

        protected override void OnStart(string[] args)
        {
            t.Start();
        }

        protected override void OnStop()
        {
            t.Stop();
        }

        void t_Elapsed(object sender,ElapsedEventArgs e)
        {
            try
            {
                if (DateTime.Now <= vig)
                {
                    CPayments pay = new CPayments();
                    pay.SentPaymentsDue();
                    pay.GetPayments ();

                  
                }

                if ((DateTime.Now.Hour >= 3 && DateTime.Now.Minute >= 00) && (DateTime.Now.Hour <= 3 && DateTime.Now.Minute <= 15))
                {
                    if (serviceIsRunning("IntSAP-DueP2"))
                    {
                        stopService("IntSAP-DueP2");
                    }
                }
                if ((DateTime.Now.Hour >= 3 && DateTime.Now.Minute >= 16) && (DateTime.Now.Hour <= 3 && DateTime.Now.Minute <= 30))
                {
                    if (!serviceIsRunning("IntSAP-DueP2"))
                    {
                       startService("IntSAP-DueP2");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
        
        /// <summary>
        ///  Verify if a service is running.
        /// </summary>
        /// <param name="ServiceName"></param>
        public bool serviceIsRunning(string ServiceName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = ServiceName;

            if (sc.Status == ServiceControllerStatus.Running)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Detener un servicio que está activo
        /// </summary>
        /// <param name="ServiceName"></param>
        public void stopService(string ServiceName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = ServiceName;

            //Console.WriteLine("The {0} service status is currently set to {1}", ServiceName, sc.Status.ToString());

            if (sc.Status == ServiceControllerStatus.Running)
            {
                // Inicie el servicio si se detiene el estado actual.
                //Console.WriteLine("Stopping the {0} service ...", ServiceName);
                try
                {
                    // Inicie el servicio y espere hasta que su estado sea "En ejecución".
                    sc.Stop();
                    sc.WaitForStatus(ServiceControllerStatus.Stopped);

                    // Muestra el estado actual del servicio.
                    //Console.WriteLine("El estado del servicio {0} ahora se establece en {1}.", ServiceName, sc.Status.ToString());
                }
                catch (InvalidOperationException e)
                {
                    //Console.WriteLine("Could not stop the {0} service.", ServiceName);
                    //Console.WriteLine(e.Message);
                    Common.LogArchivo.EscribeLog(e);
                }
            }
            else
            {
                Common.LogArchivo.EscribeLog(" No se puede detener el servicio porque ya está inactivo. " + ServiceName,01);                
            }
        }
        
        /// <summary>
        /// Iniciar un servicio por su nombre
        /// </summary>
        /// <param name="ServiceName"></param>
        public void startService(string ServiceName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = ServiceName;

            //Console.WriteLine("El estado del servicio {0} está configurado actualmente en {1}", ServiceName, sc.Status.ToString());

            if (sc.Status == ServiceControllerStatus.Stopped)
            {
                // Inicie el servicio si se detiene el estado actual.
                //Console.WriteLine("Starting the {0} service ...", ServiceName);
                try
                {
                    // Inicie el servicio y espere hasta que su estado sea "En ejecución".
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running);

                    // Muestra el estado actual del servicio.
                    //Console.WriteLine("El estado del servicio {0} ahora se establece en {1}.", ServiceName, sc.Status.ToString());
                }
                catch (InvalidOperationException e)
                {
                    Common.LogArchivo.EscribeLog(e);
                    //Console.WriteLine("No se pudo iniciar el servicio {0}.", ServiceName);
                    //Console.WriteLine(e.Message);
                }
            }
            else
            {
                Common.LogArchivo.EscribeLog(" El servicio ya se está ejecutando. " + ServiceName, 02);
                //Console.WriteLine("El servicio {0} ya se está ejecutando.", ServiceName);
            }
        }
    }
}
