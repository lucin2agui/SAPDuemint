﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace IntSAP_Due
{
    public class Conex
    {
        public SAPbouiCOM.Application oApplication = null;
        public string AddOn_Nombre = "IntSAP_Due";
        public SAPbobsCOM.Company oCompany { get; set; }
        public int conectado { get; set; }
        private SqlConnection oConnection;
        private SqlCommand oCommand;
        private SqlDataReader oReader;
        public string msj= "";
        
        public void ConCompany()
        {
            string companyDB = Properties.Settings.Default.CompanyDB;
            SAPbobsCOM.BoDataServerTypes DbServerType = (SAPbobsCOM.BoDataServerTypes)0;
            string sErrMsg = "";
            companyDB = companyDB.Trim();
            if (companyDB.Equals(""))
            {
                conectado = -1;
                msj = "No se definio base de datos, imposible conectar";
                oCompany = null;
                return;
            }
            try
            {

                if (oCompany == null)
                {
                    System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());

                    oCompany = new SAPbobsCOM.Company(); //creamos una nueva instacia del objeto company
                   
                    switch (Properties.Settings.Default.DbServerType)
                    {
                        case " MS 2005":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                            break;
                        case "MS 2008":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                            break;
                        case "MS 2012":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                            break;
                        case "MS 2014":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                            break;
                        case "MS 2016":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                            break;
                        case "MS 2017":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                            break;
                        case "MS 2019":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                            break;
                        case "HANADB":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                            break;
                    }
                    oCompany.DbServerType = DbServerType;
                    oCompany.CompanyDB = companyDB;
                    oCompany.Server = Properties.Settings.Default.Server;
                    oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Spanish;
                    oCompany.UserName = Properties.Settings.Default.SAPUserName; // "manager";
                    oCompany.Password = Properties.Settings.Default.SAPPassword; // "B1Admin";
                    //oCompany.UseTrusted = false;
                    //oCompany.DbUserName = Properties.Settings.Default.DbUserName; // "SYSTEM";
                    //oCompany.DbPassword = Properties.Settings.Default.DbPassword; // "SAPHANAsna2016";
                   
                    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
                    int returnSAPBO = oCompany.Connect();
                    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
                    conectado = returnSAPBO; 
                    if (returnSAPBO != 0)
                    {
                        oCompany.GetLastError(out returnSAPBO, out sErrMsg);
                        msj = returnSAPBO.ToString();
                        Common.LogArchivo.EscribeLog(sErrMsg, returnSAPBO, "Error Al conectar a Company:" + companyDB + " Server:" + oCompany.Server + " Usuario:" + oCompany.UserName);
                        LiberarObjetoGenerico(oCompany);
                    }
                    else
                    {
                        GC.KeepAlive(oCompany);
                        //msj = "Conectado Company Name: " + oCompany.CompanyName + " - CompanyDB:" + companyDB + " - User " + oCompany.UserName;
                        //Common.LogArchivo.EscribeLog(msj, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
        }

        public void DesCompany()
        {
            try
            {
                if ((conectado.Equals(0)))
                {
                    oCompany.Disconnect();
                    oCompany = null;
                    //Common.LogArchivo.EscribeLog("---------------------------Desconectado de la compañia-----------------------------", 0);
                }
                else
                {
                    conectado = -1;
                    oCompany = null;
                }
            }
            catch (Exception ex)
            {
                //Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public void InicarSBO()
        {
            if (oCompany != null)
            {
                oCompany.Disconnect();
                ConCompany();
            }
            else
                ConCompany();
        }

        public void LiberarObjetoGenerico(Object objeto)
        {
            try
            {
                if (objeto != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(objeto);
                    GC.Collect();
                }
            }
            finally
            {

            }
        }

        public void conectSQL()
        {
            StringBuilder dConexion = new StringBuilder();
            dConexion.AppendFormat("Server={0};", Properties.Settings.Default.Server);
            dConexion.AppendFormat("Uid={0};", Properties.Settings.Default.DbUserName);
            dConexion.AppendFormat("Pwd={0};", Properties.Settings.Default.DbPassword);
            dConexion.AppendFormat("Database={0};", Properties.Settings.Default.CompanyDB);
            this.oConnection = new SqlConnection(dConexion.ToString());
        }

        public SqlDataReader exeReader(string Query)
        {
            try
            {
                conectSQL();
                this.oConnection.Open();
                this.oReader = null;
                this.oCommand = new SqlCommand(Query, this.oConnection);
                this.oCommand.Prepare();
                this.oReader = this.oCommand.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                return this.oReader;
            }
            catch (Exception ex)
            {
                this.oConnection.Close();
                throw ex;
            }
        }

        public int ExeQuery(string Query)
        {
            int retorno = 0;
            try
            {
                conectSQL();
                this.oConnection.Open();
                this.oCommand = new SqlCommand(Query, this.oConnection);
                this.oCommand.Prepare();
                retorno = this.oCommand.ExecuteNonQuery();
                return retorno;
            }
            catch (Exception ex)
            {
                return -1;
                throw ex;
            }
            finally
            {
                this.oConnection.Close();
            }
        }
    }
}
