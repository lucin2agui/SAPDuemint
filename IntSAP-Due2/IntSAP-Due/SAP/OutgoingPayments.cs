﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.SAP
{
    public class OutgoingPayments
    {
        public void ProcessPay()
        {
            Conex con = new Conex();
            //con.InicarSBO();
            //con.ConCompany();
            //if (con.conectado.Equals(0))
            {
                //SAPbobsCOM.Recordset oRec;
                //SAPbobsCOM.Recordset oRec2;
                string Query = null;
                string Query2 = null;
                string Json = null;
                string Code = null;
                Common.items pay;
                Common.documents[] lDoc = null;
                List<Common.items> lPayment = null;
                string docentry = null;
                int j = 0;
                string folio = null;
                string Rut = null;
                try
                {
                    //oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    Query = @"  SELECT TOP 5 
                            T0.[Code], 
                            T0.[Name],
                            T0.[U_Json] 
                            FROM 
                            [@INTSAPDUE]  T0 
                            WHERE T0.[U_Status] =1
                            AND LEN(CONVERT(VARCHAR(MAX),T0.[U_Json])) > 0
                            AND T0.[Name] LIKE '%Pago desde Duemint%'
                           
                        ";
                    var oRec = con.exeReader(Query);
                    if (oRec.FieldCount > 0)
                    {
                        while (oRec.Read())
                        {
                            j = 0;
                            Code = Convert.ToString(oRec["Code"]);
                            Json = Convert.ToString(oRec["U_Json"]);
                            pay = JsonConvert.DeserializeObject<Common.items>(Json);
                            lDoc = new Common.documents[pay.documents.Length];
                            foreach (Common.documents docs in pay.documents)
                            {
                                folio = docs.document.number;
                                Rut = docs.document.Client.taxId;
                                Query2 = @" SELECT 
                                        T0.[DocEntry],
                                        T0.[DocNum],  
                                        T0.[ObjType], 
                                        T0.[CardCode], 
                                        T0.[FolioPref], 
                                        T0.[FolioNum],
                                        T0.[DocTotal],
                                        (T0.[DocTotal] - T0.[PaidToDate]) Paid
                                        FROM OINV T0 
                                        WHERE 
                                        T0.[LicTradNum]  = '" + docs.document.Client.taxId + @"' 
                                        AND T0.[FolioNum] = " + docs.document.number + @" ";
                                //oRec2.DoQuery(Query2);
                                var oRec2 = con.exeReader(Query2);
                                while (oRec2.Read())
                                {
                                    docentry = Convert.ToString(oRec2["DocEntry"]);
                                    docs.document.DocEntry = Convert.ToInt32(docentry);
                                    docs.document.Client.CardCode = Convert.ToString(oRec2["CardCode"]);
                                    docs.PayDate = pay.conciliatedDate;
                                    docs.JournalRemarks = "PAGO DUEMINT " + docs.document.Client.taxId;
                                    docs.Code = Code;
                                    lDoc[j] = docs;
                                    pay.Client.CardCode = Convert.ToString(oRec2["CardCode"]);
                                    j++;
                                }
                                //else
                                //{
                                //    j = 0;
                                //    docentry = null;
                                //}
                            }
                            pay.documents = lDoc;
                            if (!string.IsNullOrEmpty(docentry))
                            {
                                lPayment.Add(pay);
                                CreatePayments(lPayment);
                            }
                                
                        }
                    }


                        //AND T0.[Code] = '9522588'  
                        //oRec.DoQuery(Query);
                   
                    //Common.documents[] lDoc = null;
                   
                    //string docentry = null;
                    //int j = 0;
                    //oRec2 = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    //if (!oRec.EoF)
                    //{
                    //    for (int i = 0; i < oRec.RecordCount; i++)
                    //    {
                    //        //Code = Convert.ToString(oRec.Fields.Item("Code").Value);
                    //        //Json = Convert.ToString(oRec.Fields.Item("U_Json").Value);
                    //        //pay = JsonConvert.DeserializeObject<Common.items>(Json);
                    //        j = 0;
                    //        lDoc = new Common.documents[pay.documents.Length];
                    //        foreach (Common.documents docs in pay.documents)
                    //        {
                    //            var folio = docs.document.number;
                    //            var Rut = docs.document.Client.taxId;
                    //            Query2 = @" SELECT 
                    //                    T0.[DocEntry],
                    //                    T0.[DocNum],  
                    //                    T0.[ObjType], 
                    //                    T0.[CardCode], 
                    //                    T0.[FolioPref], 
                    //                    T0.[FolioNum],
                    //                    T0.[DocTotal],
                    //                    (T0.[DocTotal] - T0.[PaidToDate]) Paid
                    //                    FROM OINV T0 
                    //                    WHERE 
                    //                    T0.[LicTradNum]  = '" + docs.document.Client.taxId + @"' 
                    //                    AND T0.[FolioNum] = " + docs.document.number + @" ";
                    //            oRec2.DoQuery(Query2);
                    //            if (!oRec2.EoF)
                    //            {
                    //                docentry = Convert.ToString(oRec2.Fields.Item("DocEntry").Value);
                    //                docs.document.DocEntry = Convert.ToInt32(docentry);
                    //                docs.document.Client.CardCode = Convert.ToString(oRec2.Fields.Item("CardCode").Value);
                    //                docs.PayDate = pay.conciliatedDate;
                    //                docs.JournalRemarks = "PAGO DUEMINT " + docs.document.Client.taxId;
                    //                docs.Code = Code;
                    //                lDoc[j] = docs;
                    //                pay.Client.CardCode = Convert.ToString(oRec2.Fields.Item("CardCode").Value);
                    //                j++;

                    //            }
                    //            else
                    //            {
                    //                j = 0;
                    //                docentry = null;
                    //            }
                    //        }
                    //        pay.documents = lDoc;
                    //        if (!string.IsNullOrEmpty(docentry))
                    //            CreatePayments(pay);
                    //        oRec.MoveNext();
                    //    }

                    //}
                    //con.LiberarObjetoGenerico(oRec);
                    //con.LiberarObjetoGenerico(oRec2);
                }
                catch (Exception ex)
                {
                    Common.LogArchivo.EscribeLog(ex);
                }
                finally
                {
                    Query = null;
                    Query2 = null;
                    Json = null;
                    Code = null;
                    pay = null;
                    lDoc = null;
                    lPayment = null;
                    docentry = null;
                    j = 0;
                    folio = null;
                    Rut = null;
                    GC.Collect();
                }
            }
        }

        public void CreatePayments(List<Common.items> LPayment)
        {
            Conex con = new Conex();
            con.InicarSBO();
            //con.ConCompany();
            string Qstado = null;
            SAPbobsCOM.Recordset oRec;
            try
            {
                oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                foreach (Common.items pay in LPayment)
                {
                    SAPbobsCOM.Payments oPayments = null;
                    //oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    Common.documents[] lDoc = pay.documents;

                    oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments);
                    oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                    foreach (Common.documents doc in lDoc)
                    {
                        switch (doc.document.code)
                        {
                            case 60:
                            case 61:
                                //oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments);
                                //oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                                oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                                oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_CredItnote;
                                break;
                            case 30:
                            case 33:

                                oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                                oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                                break;
                        }
                        //oPayments.CounterReference = FolioNumber.ToString();
                        oPayments.Reference1 = Convert.ToString(pay.id);
                        oPayments.JournalRemarks = doc.JournalRemarks;
                        oPayments.Invoices.DocEntry = doc.document.DocEntry;
                        oPayments.Invoices.SumApplied = doc.amount;
                        oPayments.Invoices.Add();
                        //oPayments.Invoices.AppliedFC =                   

                        #region Efectivo
                        //oPayments.CashAccount = Properties.Settings.Default.CashAccount;
                        //oPayments.CashSum = doc.amount;
                        #endregion Efectivo

                        #region Tarjeta
                        //oPayments.CreditCards.CreditCard = 1; //tarjetas debito o credito
                        //oPayments.CreditCards.CreditCardNumber = ""; //numero de tarjeta
                        //oPayments.CreditCards.CardValidUntil = DateTime.Now; //Fecha de validez
                        //oPayments.CreditCards.VoucherNum = "";//numero de referencia
                        //oPayments.CreditCards.OwnerPhone = "";//telefono del dueño
                        //oPayments.CreditCards.CreditSum = doc.amount;
                        //oPayments.CreditCards.ConfirmationNum = "";
                        //oPayments.CreditCards.NumOfPayments = 1; //numero de pago
                        #endregion Tarjeta

                        #region Cheque
                        //oPayments.Checks.DueDate = oPay.conciliatedDate;
                        //oPayments.Checks.BankCode = oPay.bankCode;
                        //oPayments.Checks.CheckSum = doc.amount;
                        //oPayments.Checks.CountryCode = "CL";
                        //oPayments.Checks.CheckAccount = Properties.Settings.Default.CheckAccount;
                        #endregion Cheque

                    }
                    oPayments.CardCode = pay.Client.CardCode;
                    oPayments.DocDate = pay.conciliatedDate;
                    oPayments.DueDate = pay.conciliatedDate;
                    oPayments.TaxDate = pay.conciliatedDate;

                    #region Transferencia
                    oPayments.TransferDate = pay.conciliatedDate;
                    oPayments.TransferAccount = Properties.Settings.Default.TransferAccount;
                    oPayments.TransferReference = "PAGO DUEMINT " + pay.Client.taxId;
                    oPayments.TransferSum = pay.amount;
                    #endregion Transferencia

                    int inDocReg = oPayments.Add();
                    if (inDocReg != 0)
                    {
                        string sError = con.oCompany.GetLastErrorDescription();
                        Qstado = @" UPDATE T0 SET T0.[U_Status] = 3,T0.[U_Mensaje] = '" + sError + @"' FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '" + pay.id + @"'";
                        oRec.DoQuery(Qstado);
                        //con.DesCompany();
                    }
                    else
                    {
                        string DocEntr = con.oCompany.GetNewObjectKey();
                        Qstado = @" UPDATE T0 SET T0.[U_Status] = 2,T0.[U_Mensaje] = 'Creado',T0.[U_DocEntry] = " + DocEntr + @" FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '" + pay.id + @"'";
                        oRec.DoQuery(Qstado);
                        //con.DesCompany();
                    }
                }               
                con.LiberarObjetoGenerico(oRec);
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
                //con.DesCompany();
            }
            finally
            {
                //con.DesCompany();              
                GC.Collect();
            }
        }

        public void CreatePayments(Common.items pay)
        {
            Conex con = new Conex();
            con.InicarSBO();
            //con.ConCompany();
            string Qstado = null;
            SAPbobsCOM.Recordset oRec;
            try
            {               
                SAPbobsCOM.Payments oPayments = null;
                oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Common.documents[] lDoc = pay.documents;

                oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments);
                oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                foreach (Common.documents doc in lDoc)
                {
                    switch (doc.document.code)
                    {
                        case 60:
                        case 61:
                            //oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments);
                            //oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                            oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                            oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_CredItnote;
                            break;
                        case 30:
                        case 33:
                            
                            oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                            oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                            break;
                    }
                    //oPayments.CounterReference = FolioNumber.ToString();
                    oPayments.Reference1 = Convert.ToString(pay.id);
                    oPayments.JournalRemarks = doc.JournalRemarks;
                    oPayments.Invoices.DocEntry = doc.document.DocEntry;
                    oPayments.Invoices.SumApplied = doc.amount;
                    oPayments.Invoices.Add();
                    //oPayments.Invoices.AppliedFC =                   

                    #region Efectivo
                    //oPayments.CashAccount = Properties.Settings.Default.CashAccount;
                    //oPayments.CashSum = doc.amount;
                    #endregion Efectivo

                    #region Tarjeta
                    //oPayments.CreditCards.CreditCard = 1; //tarjetas debito o credito
                    //oPayments.CreditCards.CreditCardNumber = ""; //numero de tarjeta
                    //oPayments.CreditCards.CardValidUntil = DateTime.Now; //Fecha de validez
                    //oPayments.CreditCards.VoucherNum = "";//numero de referencia
                    //oPayments.CreditCards.OwnerPhone = "";//telefono del dueño
                    //oPayments.CreditCards.CreditSum = doc.amount;
                    //oPayments.CreditCards.ConfirmationNum = "";
                    //oPayments.CreditCards.NumOfPayments = 1; //numero de pago
                    #endregion Tarjeta

                    #region Cheque
                    //oPayments.Checks.DueDate = oPay.conciliatedDate;
                    //oPayments.Checks.BankCode = oPay.bankCode;
                    //oPayments.Checks.CheckSum = doc.amount;
                    //oPayments.Checks.CountryCode = "CL";
                    //oPayments.Checks.CheckAccount = Properties.Settings.Default.CheckAccount;
                    #endregion Cheque

                }
                oPayments.CardCode = pay.Client.CardCode;
                oPayments.DocDate = pay.conciliatedDate;
                oPayments.DueDate = pay.conciliatedDate;
                oPayments.TaxDate = pay.conciliatedDate;

                #region Transferencia
                oPayments.TransferDate = pay.conciliatedDate;
                oPayments.TransferAccount = Properties.Settings.Default.TransferAccount;
                oPayments.TransferReference = "PAGO DUEMINT " + pay.Client.taxId; 
                oPayments.TransferSum = pay.amount;
                #endregion Transferencia

                int inDocReg = oPayments.Add();
                if (inDocReg != 0)
                {
                    string sError = con.oCompany.GetLastErrorDescription();
                    Qstado = @" UPDATE T0 SET T0.[U_Status] = 3,T0.[U_Mensaje] = '"+ sError+@"' FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '"+ pay.id + @"'";
                    oRec.DoQuery(Qstado);
                    //con.DesCompany();
                }
                else
                {
                    string DocEntr = con.oCompany.GetNewObjectKey();
                    Qstado = @" UPDATE T0 SET T0.[U_Status] = 2,T0.[U_Mensaje] = 'Creado',T0.[U_DocEntry] = "+ DocEntr + @" FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '" + pay.id + @"'";
                    oRec.DoQuery(Qstado);
                    //con.DesCompany();
                }
                con.LiberarObjetoGenerico(oRec);
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
                //con.DesCompany();
            }
            finally
            {
                //con.DesCompany();              
                GC.Collect();
            }
        }

        public void SentPayments()
        {
            Conex con = new Conex();
            con.InicarSBO();
            //con.ConCompany();
            //if (con.conectado.Equals(0))
            {
                SAPbobsCOM.Recordset oRec;
                SAPbobsCOM.Recordset oRec2;
                string Query = null;
                string Query2 = null;
                try
                {
                    oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    Query = @"SELECT TOP 5 T0.[Code]
	                              ,T0.[U_DocEntry]
	                              ,T3.DocTotal
	                              ,T3.TaxDate
	                              ,T4.LicTradNum
                            FROM [@INTSAPDUE] T0 WITH (NOLOCK)
                            INNER JOIN ORCT T3 WITH (NOLOCK) ON CONVERT(VARCHAR(30),T3.[DocEntry]) = CONVERT(VARCHAR(30),T0.[U_DocEntry]) 
                            INNER JOIN OCRD T4 WITH (NOLOCK) ON T4.CardCode = T3.CardCode
                            WHERE T0.[U_Status] = 1
                            AND T0.[Name] LIKE '%Pago desde SAP%'
                          
                        ";

                    oRec.DoQuery(Query);
                    string Code = null;
                    string DocEntry = null;
                    double DocTotal;
                    double SumApplied;
                    CDocuments cDoc = new CDocuments();
                    IntSAP_Due.Common.Due.PayDuemint cCPay = new IntSAP_Due.Common.Due.PayDuemint();
                    if (!oRec.EoF)
                    {
                        CPayments cPayment = new CPayments();
                        oRec2 = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        for (int i = 0; i < oRec.RecordCount; i++)
                        {
                            Code = Convert.ToString(oRec.Fields.Item("Code").Value);
                            DocEntry = Convert.ToString(oRec.Fields.Item("U_DocEntry").Value);
                            DocTotal = Math.Round(Convert.ToDouble(oRec.Fields.Item("DocTotal").Value), 0);
                            cCPay = new Common.Due.PayDuemint();
                            cCPay.currency = "CLP";
                            cCPay.date = Convert.ToDateTime(oRec.Fields.Item("TaxDate").Value).ToString("yyyy-MM-dd");
                            cCPay.total = DocTotal.ToString();

                            cCPay.taxId = Convert.ToString(oRec.Fields.Item("LicTradNum").Value);
                            Query2 = @" SELECT 
	                                 T1.[DocNum]
	                                ,T1.[InvoiceId]
	                                ,T1.[DocEntry]
	                                ,T1.[SumApplied]
	                                ,T1.[AppliedSys]
	                                ,T2.[FolioNum]
                                    ,T3.[CheckSum] 
                                    ,T3.[TrsfrSum]
                                FROM RCT2 T1 WITH (NOLOCK) 
                                INNER JOIN ORCT T3 WITH (NOLOCK) ON T3.[DocEntry] = T1.[DocNum]
                                INNER JOIN OINV T2 WITH (NOLOCK) ON T2.[DocEntry] = T1.[DocEntry]
                                WHERE
                                CONVERT(VARCHAR(30),T1.[DocNum]) = CONVERT(VARCHAR(30), '" + DocEntry + @"')
                                    ";
                            oRec2.DoQuery(Query2);
                            for (int j = 0; j < oRec2.RecordCount; j++)
                            {
                                if (Convert.ToDouble(oRec2.Fields.Item("CheckSum").Value) > 0)
                                {
                                    cCPay.paymentMethod = "Cheque";
                                }
                                else
                                {
                                    cCPay.paymentMethod = "Transferencia";
                                }
                                SumApplied = Math.Round(Convert.ToDouble(oRec2.Fields.Item("SumApplied").Value.ToString()), 0);
                                cCPay.documents.Add(new IntSAP_Due.Common.Due.documents() { number = Convert.ToString(oRec2.Fields.Item("FolioNum").Value), code = "33", amount = SumApplied.ToString() });
                                oRec2.MoveNext();
                            }
                            cPayment.CreatePaymentByTaxId(cCPay, Code);
                            oRec.MoveNext();
                        }
                        con.LiberarObjetoGenerico(oRec2);
                    }

                    con.LiberarObjetoGenerico(oRec);
                   
                }
                catch (Exception ex)
                {
                    Common.LogArchivo.EscribeLog(ex);
                    //con.DesCompany();
                }
                finally
                {
                    //con.DesCompany();
                    GC.Collect();
                }
            }
        }
        
    }
}
