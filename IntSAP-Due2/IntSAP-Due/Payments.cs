﻿using IntSAP_Due.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due
{
    public class CPayments
    {
        public void GetPayments()
        {
            //string mensaje = null;
            try
            {
                var clientDowload = new RestClient();
                string URLDowload = Properties.Settings.Default.URLPay;
                string param = "?since=" + (string.IsNullOrEmpty(Properties.Settings.Default.FechaPagos) == true ? DateTime.Now.ToString("yyyy-MM-dd") : Properties.Settings.Default.FechaPagos);
                string param2 = "&dateBy=3&order=ASC&resultsPerPage=100&status=1";
                var request = new RestRequest(URLDowload+ param + param2, Method.GET);
                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Cookie", "device_view=full");
                /*Parameter Param = new Parameter();
                Param.Name = "since";
                Param.Value = (string.IsNullOrEmpty(Properties.Settings.Default.FechaPagos) == true? DateTime.Now.ToString("yyyy-MM-dd"): Properties.Settings.Default.FechaPagos);  // DateTime.Now.ToString("yyyy-MM-dd");
                request.Parameters.Add(Param);
                */
                request.RequestFormat = DataFormat.Json;
                IRestResponse responseDowload = clientDowload.Execute(request);
                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    Common.lPay lPay = JsonConvert.DeserializeObject<Common.lPay>(responseDowload.Content);
                    if (lPay.Records.items > 0)
                    {                       
                        foreach (items pay in lPay.items)
                        {
                            var josn  = JsonUtility.JsonParser.Serialize(pay,true);
                            LOGSAP log = new LOGSAP();
                            INTSAPDUE oTableLog = new INTSAPDUE();
                            oTableLog.Code = pay.id.ToString();
                            oTableLog.Name = "Pago desde Duemint " +pay.id.ToString();
                            oTableLog.U_IdDueMint = pay.id;
                            oTableLog.U_ObjType = 24;
                            oTableLog.U_Json = josn.ToString();
                            oTableLog.U_Status = 1;
                            log.AddLogPay(oTableLog);
                            //log.AddLogSap(oTableLog);
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                GC.Collect();
            }
        }
        
        public void CreatePaymentByTaxId(IntSAP_Due.Common.Due.PayDuemint cCPay, string code)
        {           
            try
            {
                string body = JsonConvert.SerializeObject(cCPay);

                IntSAP_Due.Common.Paydownload.Paydownload paydownload = new IntSAP_Due.Common.Paydownload.Paydownload();
                string URLCP = Properties.Settings.Default.URLCPBT;
                var clientRes = new RestClient(URLCP);                
                var request = new RestRequest( Method.POST);
                request.AddHeader("Authorization", "Bearer " + Properties.Settings.Default.Token);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Cookie", "device_view=full");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                //request.RequestFormat = DataFormat.Json;
                //request.AddHeader("Accept", "application/json");
                //request.AddParameter("taxId", cCPay.taxId.ToString());
                //request.AddParameter("date", cCPay.date);
                //request.AddParameter("total", cCPay.total);
                //request.AddParameter("currency", cCPay.currency);
                //string doc = JsonConvert.SerializeObject(cCPay.documents).ToString();
                //request.AddParameter("documents", doc.Replace(@"\", ""));

                //request.AddBody(JsonPay);

                IRestResponse responseDowload = clientRes.Execute(request);
                if (responseDowload.StatusDescription.Equals("OK"))
                {
                    paydownload = JsonConvert.DeserializeObject<IntSAP_Due.Common.Paydownload.Paydownload>(responseDowload.Content);
                    if (paydownload.id > 0)
                    {
                        var josn = JsonUtility.JsonParser.Serialize(paydownload, true);
                        LOGSAP log = new LOGSAP();
                        INTSAPDUE oTableLog = new INTSAPDUE();
                        oTableLog.Code = code;
                        oTableLog.U_IdDueMint = paydownload.id;
                        oTableLog.U_Mensaje = "Creado";
                        oTableLog.U_Json = josn.ToString();
                        oTableLog.U_JsonSent = body;
                        oTableLog.U_Status = 2;
                        log.UpdateLogSap(oTableLog);
                    }
                    else
                    {
                        var mens = JsonConvert.DeserializeObject<IntSAP_Due.Common.Error>(responseDowload.Content);

                        LOGSAP log = new LOGSAP();
                        INTSAPDUE oTableLog = new INTSAPDUE();
                        oTableLog.Code = code;
                        oTableLog.U_IdDueMint = 0;
                        oTableLog.U_Mensaje = "Enviado sin respuesta";
                        oTableLog.U_Json = "";
                        oTableLog.U_JsonSent = body;
                        oTableLog.U_Status = 2;
                        log.UpdateLogSap(oTableLog);
                    }
                }
                else
                {
                    var mens = JsonConvert.DeserializeObject<IntSAP_Due.Common.Error>(responseDowload.Content);

                    LOGSAP log = new LOGSAP();
                    INTSAPDUE oTableLog = new INTSAPDUE();
                    oTableLog.Code = code;
                    oTableLog.U_IdDueMint = 0;
                    oTableLog.U_Mensaje = "Error:" + mens.error + " mensaje :" + mens.message;
                    oTableLog.U_Json = "";
                    oTableLog.U_JsonSent = body;
                    oTableLog.U_Status = 3;
                    log.UpdateLogSap(oTableLog);
                }
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
