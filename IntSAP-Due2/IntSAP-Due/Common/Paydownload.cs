﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common.Paydownload
{
    public class Paydownload
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public string createdBy { get; set; }
        public string conciliatedBy { get; set; }
        public DateTime conciliatedDate { get; set; }
        public DateTime date { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string bankCode { get; set; }
        public string paymentMethod { get; set; }
        public string transactionCode { get; set; }
        public string documentNumber { get; set; }
        public string comments { get; set; }
        public string internalCode { get; set; }
        public List<IntSAP_Due.Common.Paydownload.clients> clients { get; set; }
        public IntSAP_Due.Common.Paydownload.clients client { get; set; }
        public string[] documents { get; set; }
        public string[] files { get; set; }
    }
    public class clients
    {
        public string id { get; set; }
        public string name { get; set; }
        public string taxId { get; set; }
    }
}
