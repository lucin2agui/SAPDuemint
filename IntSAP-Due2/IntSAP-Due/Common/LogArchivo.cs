﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common
{
    class LogArchivo
    {
        private static string strLogFilePath = string.Empty;
        private static StreamWriter sw = null;

        private static string LogFilePath
        {
            set
            {
                strLogFilePath = value;
            }
            get
            {
                return strLogFilePath;
            }
        }

        public static void ReporteLog(string MensajeSAP, string textoLibre)
        {
            try
            {
                if (Properties.Settings.Default.LogGeneral.Equals(true))
                {
                    Exception objException = null;
                    bool resultado = RegistoError(objException, MensajeSAP, 0, textoLibre);
                }
            }
            catch
            {
            }
        }

        public static void EscribeLog(Exception objException)
        {
            try
            {
                bool resultado = RegistoError(objException, "", 0, "");
            }
            catch
            {
            }

        }

        public static void EscribeLog(string MensajeSAP, int CodErrorSAP)
        {
            try
            {
                Exception objException = null;
                bool resultado = RegistoError(objException, MensajeSAP, CodErrorSAP, "");
            }
            catch
            {
            }
        }

        public static void EscribeLog(string MensajeSAP, int CodErrorSAP, string textoLibre)
        {
            try
            {
                Exception objException = null;
                bool resultado = RegistoError(objException, MensajeSAP, CodErrorSAP, textoLibre);
            }
            catch
            {
            }
        }

        public static void EscribeLog(Exception objException, string MensajeSAP, int CodErrorSAP, string textoLibre)
        {
            try
            {
                bool resultado = RegistoError(objException, MensajeSAP, CodErrorSAP, textoLibre);
            }
            catch
            {
            }

        }


        private static bool RegistoError(Exception objException, string MensajeSAP, int CodErrorSAP, string textoLibre)
        {
            string strPathName = string.Empty;

            if (strLogFilePath.Equals(string.Empty))
            {
                //Get Default log file path "LogFile.txt"
                strPathName = GetLogFilePath();
            }
            else
            {
                //If the log file path is not empty but the file
                //is not available it will create it
                if (true != File.Exists(strLogFilePath))
                {
                    if (false == CheckDirectory(strLogFilePath))
                        return false;

                    FileStream fs = new FileStream(strLogFilePath,
                            FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();
                }
                strPathName = strLogFilePath;
            }
            bool bReturn = true;

            // write the error log to that text file
            if (true != WriteErrorLog(strPathName, objException, MensajeSAP, CodErrorSAP, textoLibre))
            {
                bReturn = false;
            }
            return bReturn;
        }

        private static bool CheckDirectory(string strLogFilePath)
        {
            try
            {
                int nFindSlashPos = strLogFilePath.Trim().LastIndexOf("\\");
                string strDirectoryname = strLogFilePath.Trim().Substring(0, nFindSlashPos);

                if (false == Directory.Exists(strDirectoryname))
                    Directory.CreateDirectory(strDirectoryname);
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }        

        private static string GetLogFilePath()
        {
            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();
                if (sDay.Length.Equals(1)) sDay = "0" + sDay;
                if (sMonth.Length.Equals(1)) sMonth = "0" + sMonth;

                string sErrorTime = sYear + "_" + sMonth + "_" + sDay;

                string baseDir = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath + @"log_proceso\";

                //  EliminaArchivo(baseDir);

                string retFilePath = baseDir + "//" + "LogFile_" + sErrorTime + ".txt";

                // si existe retorna la ruta
                if (File.Exists(retFilePath) == true)
                    return retFilePath;

                // si no existe lo crea y retorna ruta
                else
                {
                    if (false == CheckDirectory(retFilePath))
                        return string.Empty;

                    FileStream fs = new FileStream(retFilePath,
                          FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();
                }

                return retFilePath;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        
        private static bool WriteErrorLog(string strPathName, Exception objException, string MensajeSAP, int CodErrorSAP, string textoLibre)
        {

            string strException = string.Empty;

            try
            {

                sw = new StreamWriter(strPathName, true);

                if (CodErrorSAP.Equals(0))
                {
                    sw.WriteLine("----- Reporte Informativo --------");
                }
                else
                {
                    sw.WriteLine("----- Reporte Error       --------");
                }
                sw.WriteLine("Time          : " + DateTime.Now.ToLongTimeString());
                sw.WriteLine("Date          : " + DateTime.Now.ToShortDateString());

                if (!(objException == null))
                {
                    sw.WriteLine("Source        : " + objException.Source.ToString().Trim());
                    sw.WriteLine("Method        : " + objException.TargetSite.Name.ToString());

                    sw.WriteLine("Computer      : " + Dns.GetHostName().ToString());
                    sw.WriteLine("Error         : " + objException.Message.ToString().Trim());
                    sw.WriteLine("Stack Trace   : " + objException.StackTrace.ToString().Trim());
                }

                if (!(MensajeSAP.Equals("")))
                {
                    sw.WriteLine("SAP Msg       : " + MensajeSAP);
                    sw.WriteLine("SAP Cod       : " + CodErrorSAP.ToString());
                }
                if (!(textoLibre.Equals("")))
                    sw.WriteLine("Msg Adicional : " + textoLibre);


                //     sw.WriteLine("-------------------------------------------------------------------");
                sw.Flush();
                sw.Close();

                return true;
            }
            catch (Exception er)
            {
                try
                {
                    LogArchivo.EscribeLog(er);
                    return false;
                }
                catch
                {

                    return false;
                }

            }

        }
    }
}
