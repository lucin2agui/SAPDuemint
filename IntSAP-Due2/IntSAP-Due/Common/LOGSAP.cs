﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common
{
    public class LOGSAP
    {
        public void AddLogSap(INTSAPDUE intSapDue)
        {
            Conex con = new Conex();
            //if(con.conectado.Equals(-1))
            con.InicarSBO();
            //if (con.conectado.Equals(0))
            {
                SAPbobsCOM.Recordset oRec;
                oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRec.DoQuery(" SELECT COUNT(T0.[U_IdDueMint]) AS CANT FROM[dbo].[@INTSAPDUE] T0 WHERE T0.[U_IdDueMint] = " + intSapDue.U_IdDueMint + @" ");
                if (oRec.Fields.Item("CANT").Value.Equals(0))
                {
                    SAPbobsCOM.GeneralService oDocGeneralService = null;
                    SAPbobsCOM.CompanyService oCompService = null;
                    SAPbobsCOM.GeneralData oDocGeneralData = null;
                    try
                    {
                        try
                        {
                            #region Head
                            con.oCompany.StartTransaction();
                            oCompService = con.oCompany.GetCompanyService();
                            oDocGeneralService = oCompService.GetGeneralService("INTSAPDUE");
                            oDocGeneralData = (SAPbobsCOM.GeneralData)oDocGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                            oDocGeneralData.SetProperty("Code", intSapDue.Code);
                            oDocGeneralData.SetProperty("Name", intSapDue.Name);
                            oDocGeneralData.SetProperty("U_IdDueMint", intSapDue.U_IdDueMint);
                            oDocGeneralData.SetProperty("U_DocEntry", intSapDue.U_DocEntry);
                            oDocGeneralData.SetProperty("U_ObjType", intSapDue.U_ObjType);
                            oDocGeneralData.SetProperty("U_Json", intSapDue.U_Json);
                            #endregion Head

                            oDocGeneralService.Add(oDocGeneralData);
                            if (con.oCompany.InTransaction)
                            {
                                con.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            }
                            Marshal.ReleaseComObject(oCompService);
                            oCompService = null;
                            GC.Collect();
                        }
                        catch (Exception ex)
                        {
                            if (!ex.Message.Contains("Esta entrada ya existe en las tablas siguientes"))
                                Common.LogArchivo.EscribeLog(ex);
                            if (con.oCompany.InTransaction)
                            {
                                con.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            }
                            //con.DesCompany();
                        }

                    }
                    finally
                    {
                        //con.DesCompany();
                        GC.Collect();
                        con.LiberarObjetoGenerico(con.oCompany);
                        con.LiberarObjetoGenerico(oDocGeneralService);
                        con.LiberarObjetoGenerico(oCompService);
                        con.LiberarObjetoGenerico(oDocGeneralData);
                        oDocGeneralService = null;
                        oCompService = null;
                        oDocGeneralData = null;
                        con.LiberarObjetoGenerico(oRec);
                    }
                }
            }
        }

        public bool UpdateLogSap(INTSAPDUE oTableLog)
        {
            Conex con = new Conex(); 
            con.InicarSBO();
            //con.ConCompany();
            bool flag = false;
            SAPbobsCOM.GeneralService generalService = null;
            SAPbobsCOM.GeneralDataParams dataInterface = null;
            SAPbobsCOM.GeneralData byParams = null;
            try
            {
                try
                {
                    generalService = con.oCompany.GetCompanyService().GetGeneralService("INTSAPDUE");
                    dataInterface = (SAPbobsCOM.GeneralDataParams)((dynamic)generalService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams));
                    dataInterface.SetProperty("Code", oTableLog.Code);
                    byParams = generalService.GetByParams(dataInterface);                  
                    byParams.SetProperty("U_IdDueMint", oTableLog.U_IdDueMint);
                    byParams.SetProperty("U_Json", oTableLog.U_Json);
                    byParams.SetProperty("U_JsonSent", oTableLog.U_JsonSent);
                    byParams.SetProperty("U_Status", oTableLog.U_Status.ToString());
                    byParams.SetProperty("U_Mensaje", oTableLog.U_Mensaje.ToString());
                    generalService.Update(byParams);
                    flag = true;
                }
                catch (Exception ex)
                {                    
                    flag = false;
                    Common.LogArchivo.EscribeLog(ex);
                }
            }
            catch (Exception ex2)
            {
                int ExeQ = 0;
                string Query = @" UPDATE T0 SET [U_IdDueMint] = " + oTableLog.U_IdDueMint + @" ,
                                [U_Json] = '" + oTableLog.U_Json + @"',
                                [U_JsonSent] = '" + oTableLog.U_JsonSent + @"',
                                [U_Status] = '" + oTableLog.U_Status + @"',
                                [U_Mensaje] = '" + oTableLog.U_Mensaje + @"'
                                FROM [@INTSAPDUE]  T0 WHERE T0.[Code] = '" + oTableLog.Code + @"' ";
                ExeQ = con.ExeQuery(Query);
                Common.LogArchivo.EscribeLog(ex2);
                Common.LogArchivo.EscribeLog(" Fallo al actualizar pago : ",ExeQ);
            }
            finally
            {
                generalService = null;
                dataInterface = null;
                byParams = null;
                //con.DesCompany();
                GC.Collect();                
            }
            return flag;
        }

        public void AddLogPay(INTSAPDUE intSapDue)
        {
            Conex con = new Conex();
            string Query = " SELECT COUNT(T0.[U_IdDueMint]) AS CANT FROM [dbo].[@INTSAPDUE] T0 WHERE T0.[U_IdDueMint] = " + intSapDue.U_IdDueMint + @" ";
            string Query2 = null;
            var cant = con.exeReader(Query);
            int ExeQ = 0;
            if (cant.Read())
            {
                if (cant[0].ToString().Equals("0"))
                {
                    Query2 = @" INSERT INTO [dbo].[@INTSAPDUE]
                               ([DocEntry]    
                               ,[Code]
				               ,[Name]   
				               ,[U_IdDueMint]
				               ,[U_DocEntry]
				               ,[U_ObjType] 
				               ,[U_Json])

                             VALUES
                                   ( (SELECT isnull(MAX(DocEntry),0) FROM [@INTSAPDUE]) + 1                            
                                    ,'" + intSapDue.Code + @"'
                                    ,'" + intSapDue.Name + @"'
                                    ," + intSapDue.U_IdDueMint + @"
                                    ," + intSapDue.U_DocEntry + @"
                                    ," + intSapDue.U_ObjType + @"
                                    ,'" + intSapDue.U_Json + @"'
                                   );";
                    ExeQ = con.ExeQuery(Query2);
                }
            }
        }

    }
}

