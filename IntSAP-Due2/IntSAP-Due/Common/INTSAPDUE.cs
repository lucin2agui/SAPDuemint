﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_Due.Common
{
    public class INTSAPDUE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int U_IdDueMint { get; set; }
        public int U_DocEntry { get; set; }
        public int U_ObjType { get; set; }
        public string U_Mensaje { get; set; }
        public string U_Json { get; set; }
        public string U_JsonSent { get; set; }
        public int U_Status { get; set; }
        //SELECT T0.[Code], T0.[Name], T0.[U_IdDueMint], T0.[U_DocEntry], T0.[U_ObjType], T0.[U_Mensaje], T0.[U_Json] FROM [dbo].[@INTSAPDUE]  T0
    }
}
