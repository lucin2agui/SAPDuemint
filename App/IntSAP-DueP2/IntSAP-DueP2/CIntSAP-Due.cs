﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP2
{
    public class CIntSAP_Due
    {     

        private DateTime vig = new DateTime(9999, 12, 31);

        public void ProcessPay()
        {
            if (DateTime.Now <= vig)
            {
                Conex con = new Conex();
                {
                    string Query = null;
                    string Query2 = null;
                    string Json = null;
                    string Code = null;
                    Common.items pay;
                    Common.documents[] lDoc = null;
                    List<Common.items> lPayment = new List<Common.items>();
                    string docentry = null;
                    int j = 0;
                    string folio = null;
                    string Rut = null;
                    string Indicator = null;
                    try
                    {
                        Query = @"  SELECT TOP 20
                            T0.[Code], 
                            T0.[Name],
                            T0.[U_Json] 
                            FROM 
                            [@INTSAPDUE]  T0 
                            WHERE T0.[U_Status] =1
                            AND LEN(CONVERT(VARCHAR(MAX),T0.[U_Json])) > 0
                            AND T0.[Name] LIKE '%Pago desde Duemint%'
                           
                        ";
                        var oRec = con.exeReader(Query);
                        if (oRec.HasRows)
                        {
                            while (oRec.Read())
                            {
                                j = 0;
                                Code = Convert.ToString(oRec["Code"]);
                                Json = Convert.ToString(oRec["U_Json"]);
                                pay = JsonConvert.DeserializeObject<Common.items>(Json);
                                lDoc = new Common.documents[pay.documents.Length];
                                foreach (Common.documents docs in pay.documents)
                                {
                                    if (!string.IsNullOrEmpty(docs.document.code.ToString()))
                                    {
                                        folio = docs.document.number;
                                        Rut = docs.document.Client.taxId;
                                        Indicator = docs.document.code.ToString();

                                        switch (Indicator)
                                        {
                                            case "60":
                                            case "61":
                                                Query2 = @" SELECT 
                                                T0.[DocEntry],
                                                T0.[DocNum],  
                                                T0.[ObjType], 
                                                T0.[CardCode], 
                                                T0.[FolioPref], 
                                                T0.[FolioNum],
                                                T0.[DocTotal],
                                                (T0.[DocTotal] - T0.[PaidToDate]) Paid
                                                FROM ORIN T0 
                                                WHERE 
                                                T0.[LicTradNum]  = '" + docs.document.Client.taxId + @"' 
                                                AND T0.[FolioNum] = " + docs.document.number + @" ";
                                                break;
                                            default:
                                                Query2 = @" SELECT 
                                                T0.[DocEntry],
                                                T0.[DocNum],  
                                                T0.[ObjType], 
                                                T0.[CardCode], 
                                                T0.[FolioPref], 
                                                T0.[FolioNum],
                                                T0.[DocTotal],
                                                (T0.[DocTotal] - T0.[PaidToDate]) Paid
                                                FROM OINV T0 
                                                WHERE 
                                                T0.[LicTradNum]  = '" + docs.document.Client.taxId + @"' 
                                                AND T0.[FolioNum] = " + docs.document.number + @" ";
                                                break;
                                        }
                                      

                                        var oRec2 = con.exeReader(Query2);
                                        while (oRec2.Read())
                                        {
                                            docentry = Convert.ToString(oRec2["DocEntry"]);
                                            docs.document.DocEntry = Convert.ToInt32(docentry);
                                            docs.document.Client.CardCode = Convert.ToString(oRec2["CardCode"]);
                                            docs.PayDate = pay.conciliatedDate;
                                            docs.JournalRemarks = "PAGO DUEMINT " + docs.document.Client.taxId;
                                            docs.Code = Code;
                                            lDoc[j] = docs;
                                            pay.Client.CardCode = Convert.ToString(oRec2["CardCode"]);
                                            j++;
                                        }
                                        if (!oRec2.HasRows)
                                        {
                                            j = 0;
                                            docentry = null;
                                        }
                                    }
                                }
                                pay.documents = lDoc;

                                if (!string.IsNullOrEmpty(docentry))
                                {
                                    lPayment.Add(pay);
                                }

                            }
                            if (lPayment.Count > 0)
                                CreatePayments(lPayment);
                        }

                    }
                    catch (Exception ex)
                    {
                        Common.LogArchivo.EscribeLog(ex);
                    }
                    finally
                    {
                        Query = null;
                        Query2 = null;
                        Json = null;
                        Code = null;
                        pay = null;
                        lDoc = null;
                        lPayment = null;
                        docentry = null;
                        j = 0;
                        folio = null;
                        Rut = null;
                        GC.Collect();
                    }
                }
            }
        }

        public void CreatePayments(List<Common.items> LPayment)
        {
            Conex con = new Conex();
            con.InicarSBO();
            string Qstado = null;
            SAPbobsCOM.Recordset oRec;
            try
            {
                oRec = (SAPbobsCOM.Recordset)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                foreach (Common.items pay in LPayment)
                {
                    SAPbobsCOM.Payments oPayments = null;
                    Common.documents[] lDoc = pay.documents;

                    oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments);
                    oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                    foreach (Common.documents doc in lDoc)
                    {
                        switch (doc.document.code)
                        {
                            case 60:
                            case 61:
                                //oPayments = (SAPbobsCOM.Payments)con.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments);
                                //oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                                oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                                oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_CredItnote;
                                break;
                            default:
                            //case 30:
                            //case 33:

                                oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                                oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                                break;
                        }
                        //oPayments.CounterReference = FolioNumber.ToString();
                        oPayments.Reference1 = Convert.ToString(pay.id);
                        oPayments.JournalRemarks = doc.JournalRemarks;
                        oPayments.Invoices.DocEntry = doc.document.DocEntry;
                        oPayments.Invoices.SumApplied = doc.amount;
                        oPayments.Invoices.Add();    
                    }
                    oPayments.CardCode = pay.Client.CardCode;
                    oPayments.DocDate = pay.conciliatedDate;
                    oPayments.DueDate = pay.conciliatedDate;
                    oPayments.TaxDate = pay.conciliatedDate;

                    #region Transferencia
                    oPayments.TransferDate = pay.conciliatedDate;
                    oPayments.TransferAccount = Properties.Settings.Default.TransferAccount;
                    oPayments.TransferReference = "PAGO DUEMINT " + pay.Client.taxId;
                    oPayments.TransferSum = pay.amount;
                    #endregion Transferencia

                    int inDocReg = oPayments.Add();
                    if (inDocReg != 0)
                    {
                        string sError = con.oCompany.GetLastErrorDescription();
                        Qstado = @" UPDATE T0 SET T0.[U_Status] = 3,T0.[U_Mensaje] = '" + sError + @"', UpdateDate = GETDATE(), UpdateTime = CONCAT(DATEPART(hour,GETDATE()) , DATEPART(minute,GETDATE())) FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '" + pay.id + @"'";
                        oRec.DoQuery(Qstado);
                    }
                    else
                    {
                        string DocEntr = con.oCompany.GetNewObjectKey();
                        Qstado = @" UPDATE T0 SET T0.[U_Status] = 2,T0.[U_Mensaje] = 'Creado',T0.[U_DocEntry] = " + DocEntr + @", UpdateDate = GETDATE(), UpdateTime = CONCAT(DATEPART(hour,GETDATE()) , DATEPART(minute,GETDATE()))  FROM [@INTSAPDUE] T0 WHERE T0.[Code] = '" + pay.id + @"'";
                        oRec.DoQuery(Qstado);
                    }
                }
                con.LiberarObjetoGenerico(oRec);
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
            finally
            {
                con.DesCompany();
                GC.Collect();
            }
        }

    }
}
