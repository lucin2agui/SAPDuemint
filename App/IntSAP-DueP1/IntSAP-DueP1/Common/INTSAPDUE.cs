﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP1.Common
{
    public class INTSAPDUE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int U_IdDueMint { get; set; }
        public int U_DocEntry { get; set; }
        public int U_ObjType { get; set; }
        public string U_Mensaje { get; set; }
        public string U_Json { get; set; }
        public string U_JsonSent { get; set; }
        public int U_Status { get; set; }
    }
}
