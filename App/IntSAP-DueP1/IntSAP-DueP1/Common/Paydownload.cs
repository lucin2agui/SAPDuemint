﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP1.Common.Paydownload
{
    public class Paydownload
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public string createdBy { get; set; }
        public string conciliatedBy { get; set; }
        public DateTime conciliatedDate { get; set; }
        public DateTime date { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string bankCode { get; set; }
        public string accountNumber { get; set; }
        public string paymentMethod { get; set; }
        public string transactionCode { get; set; }
        public string documentNumber { get; set; }
        public string comments { get; set; }
        public string internalCode { get; set; }
        public List<Client> clients { get; set; }
        public List<DocumentItem> documents { get; set; }
        //public string[] files { get; set; }
        public Paydownload()
        {
            clients = new List<Client>();
            documents = new List<DocumentItem>();
        }
    }
    public class Client
    {
        public string id { get; set; }
        public string name { get; set; }
        public string taxId { get; set; }
    }
    public class DocumentItem
    {
        public string amount { get; set; }
        public Document document { get; set; }
    }

    public class Document
    {
        public string id { get; set; }
        public string number { get; set; }
        public int code { get; set; }
        public Client client { get; set; }
    }
}
