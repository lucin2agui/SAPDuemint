﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP1.Common
{
    public class LOGSAP
    {
        public void AddLogPay(INTSAPDUE intSapDue)
        {
            Conex con = new Conex();
            string Query = " SELECT COUNT(T0.[U_IdDueMint]) AS CANT FROM [dbo].[@INTSAPDUE] T0 WHERE T0.[U_IdDueMint] = " + intSapDue.U_IdDueMint + @" ";
            string Query2 = null;
            var cant = con.exeReader(Query);
            int ExeQ = 0;
            if (cant.Read())
            {
                if (cant[0].ToString().Equals("0"))
                {
                    Query2 = @" INSERT INTO [dbo].[@INTSAPDUE]
                               ([DocEntry]    
                               ,[Code]
				               ,[Name]   
				               ,[U_IdDueMint]
				               ,[U_DocEntry]
				               ,[U_ObjType] 
				               ,[U_Json]
                               ,[CreateDate]
				               ,[CreateTime] )

                             VALUES
                                   ( (SELECT isnull(MAX(DocEntry),0) FROM [@INTSAPDUE]) + 1                            
                                    ,'" + intSapDue.Code + @"'
                                    ,'" + intSapDue.Name + @"'
                                    ," + intSapDue.U_IdDueMint + @"
                                    ," + intSapDue.U_DocEntry + @"
                                    ," + intSapDue.U_ObjType + @"
                                    ,'" + intSapDue.U_Json.Replace("'"," ") + @"'
                                    ,GETDATE()
				                    ,CONCAT(DATEPART(hour,GETDATE()) , DATEPART(minute,GETDATE()))
                                   );";
                    ExeQ = con.ExeQuery(Query2);
                }
            }
        }
        public void UpdateLogSQL(INTSAPDUE oTableLog)
        {
            try
            {
                Conex con = new Conex();
                int ExeQ = 0;
                string Query = @" UPDATE T0 SET [U_IdDueMint] = " + oTableLog.U_IdDueMint + @" ,
                                [U_Json] = '" + oTableLog.U_Json.Replace("'"," ") + @"',
                                [U_JsonSent] = '" + oTableLog.U_JsonSent.Replace("'", " ") + @"',
                                [U_Status] = '" + oTableLog.U_Status + @"',
                                [U_Mensaje] = '" + oTableLog.U_Mensaje + @"',
                                [UpdateDate] = GETDATE(),
                                [UpdateTime] = CONCAT(DATEPART(hour,GETDATE()) , DATEPART(minute,GETDATE()))
                                FROM [@INTSAPDUE]  T0 WHERE T0.[Code] = '" + oTableLog.Code + @"' ";
                ExeQ = con.ExeQuery(Query);
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
            }
        }
    }
}
