﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP1.Common
{
    public class items
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public string createdBy { get; set; }
        public string conciliatedBy { get; set; }
        public DateTime conciliatedDate { get; set; }
        public DateTime date { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string bankCode { get; set; }
        public string paymentMethod { get; set; }
        public string transactionCode { get; set; }
        public string documentNumber { get; set; }
        public string comments { get; set; }
        public string internalCode { get; set; }
        public client Client { get; set; }
        public documents[] documents { get; set; }
        public string[] files { get; set; }


    }

    public class client
    {
        public string id { get; set; }
        public string name { get; set; }
        public string taxId { get; set; }
        public string CardCode { get; set; }

    }

    public class documents
    {
        public double amount { get; set; }
        public document document { get; set; }
        public DateTime PayDate { get; set; }
        public string JournalRemarks { get; set; }
        public string Code { get; set; }
    }

    public class document
    {
        public string id { get; set; }
        public string number { get; set; }
        public int? code { get; set; }
        public string typeDoc { get; set; } // Tipo de documento SAP
        public int DocEntry { get; set; } //DocEntry SAP
        public client Client { get; set; }

    }

    public class records
    {
        public int totalRecords { get; set; }
        public int items { get; set; }
        public int page { get; set; }
        public int pages { get; set; }
    }

    public class lPay
    {
        public records Records { get; set; }
        public IList<items> items { get; set; }
    }
}
