﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntSAP_DueP1
{
    public class Conex
    {
        public string AddOn_Nombre = "IntSAP_Due1";
        public int conectado { get; set; }
        private SqlConnection oConnection;
        private SqlCommand oCommand;
        private SqlDataReader oReader;
        public string msj = "";
        public void LiberarObjetoGenerico(Object objeto)
        {
            try
            {
                if (objeto != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(objeto);
                    GC.Collect();
                }
            }
            finally
            {

            }
        }

        public void conectSQL()
        {
            StringBuilder dConexion = new StringBuilder();
            dConexion.AppendFormat("Server={0};", Properties.Settings.Default.Server);
            dConexion.AppendFormat("Uid={0};", Properties.Settings.Default.DbUserName);
            dConexion.AppendFormat("Pwd={0};", Properties.Settings.Default.DbPassword);
            dConexion.AppendFormat("Database={0};", Properties.Settings.Default.CompanyDB);
            this.oConnection = new SqlConnection(dConexion.ToString());
        }

        public SqlDataReader exeReader(string Query)
        {
            this.oReader = null;
            try
            {
                conectSQL();
                this.oConnection.Open();               
                this.oCommand = new SqlCommand(Query, this.oConnection);
                this.oCommand.Prepare();
                this.oReader = this.oCommand.ExecuteReader(System.Data.CommandBehavior.CloseConnection);              
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
                this.oConnection.Close();               
            }
            return this.oReader;
        }

        public int ExeQuery(string Query)
        {
            int retorno = 0;
            try
            {
                conectSQL();
                this.oConnection.Open();
                this.oCommand = new SqlCommand(Query, this.oConnection);
                this.oCommand.Prepare();
                retorno = this.oCommand.ExecuteNonQuery();
                return retorno;
            }
            catch (Exception ex)
            {
                Common.LogArchivo.EscribeLog(ex);
                return -1;                
            }
            finally
            {
                this.oConnection.Close();
            }
        }
    }
}
